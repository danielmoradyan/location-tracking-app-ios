//
//  MapsViewController.swift
//  Location Sensor
//
//  Created by Daniel Moradyan on 21/03/16.
//  Copyright © 2016 Daniel Moradyan. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit


class MapsViewController: UIViewController , CLLocationManagerDelegate, MKMapViewDelegate
{
    
    var manager:CLLocationManager!
    var myLocations: [CLLocation] = []
    var c: CGFloat = 1.0
    

    @IBOutlet weak var theMap: MKMapView!
    
    @IBAction func startButtonPressed(sender: AnyObject) {
        setupManager()
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMapView()
    }
    
    
    
    func setupManager(){
        manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
    }
    
    
    
    
    func setupMapView(){
        theMap.delegate = self
        theMap.mapType = MKMapType.Standard
        theMap.annotations
        theMap.showsUserLocation = true
    }

    
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        myLocations.append(locations[0] as CLLocation)
        
        let spanX = 0.0057
        let spanY = 0.0057
        
        let newRegion = MKCoordinateRegion(center: theMap.userLocation.coordinate, span: MKCoordinateSpanMake(spanX, spanY))
        
        theMap.setRegion(newRegion, animated: true)
        
        if (myLocations.count > 1){
            let sourceIndex = myLocations.count - 1
            let destinationIndex = myLocations.count - 2
            
            let c1 = myLocations[sourceIndex].coordinate
            let c2 = myLocations[destinationIndex].coordinate
            var a = [c1, c2]
            
            let polyline = MKPolyline(coordinates: &a, count: a.count)
            theMap.addOverlay(polyline)
        }
    }
    
    
    func mapView(mapView: MKMapView, rendererForOverlay overlay: MKOverlay) -> MKOverlayRenderer {
        
        if overlay is MKPolyline {
            let polylineRenderer = MKPolylineRenderer(overlay: overlay)
            
            polylineRenderer.strokeColor = UIColor.redColor()

            polylineRenderer.lineWidth = 8.0
            return polylineRenderer
        }
        
        return MKPolygonRenderer()
    }
    
    
    @IBAction func startTrackingBtn(sender: AnyObject) {
    
    }
    
}
