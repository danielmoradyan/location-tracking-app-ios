//
//  LocationInfoViewController.swift
//  Location Sensor
//
//  Created by Daniel Moradyan on 21/03/16.
//  Copyright © 2016 Daniel Moradyan. All rights reserved.
//

import UIKit
import CoreLocation
//import MapKit
//import WatchConnectivity
//import WatchKit

class LocationInfoViewController: UIViewController, CLLocationManagerDelegate {
    
    var manager:CLLocationManager!
    var myLocations: [CLLocation] = []

    @IBOutlet weak var latLabel: UILabel!
    @IBOutlet weak var longLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupManager()
    }
    
    func setupManager(){
        manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        latLabel.text = "\(locations[0])"
        
        let location = locations[0]
        print(location.coordinate.longitude)
        print("   ")
    
        
        latLabel.text = "\(location.coordinate.latitude)"
        longLabel.text = "\(location.coordinate.longitude)"
    }
    
    
    /*
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    */
 
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
